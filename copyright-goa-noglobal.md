% Guerilla Open Access in the (no)global copyright wars
% Federico Leva
% 2022-12-10

# Guerilla Open Access in the (no)global copyright wars

## Intro

federicoleva.eu:

- Free software and free culture since ~2005
- Ecological commons (information, climate)
- Digital libraries, software development
- EU policy

Note:

- This presentation under CC BY-SA 4.0
- Not legal advice!

## What is copyright

- Don't say intellectual property
- A privilege from the king
- A state-created monopoly
- State-sanctioned violence?

## Why copyright

- Economic importance
- Geopolitical importance
- Social importance
- You can do something about it

## The example of Aaron Swartz

- 1986-2013
- [Guerilla Open Access Manifesto](https://archive.org/stream/GuerillaOpenAccessManifesto/Goamjuly2008_djvu.txt), 2008
- Automation, injustice
- JSTOR, 2011

## Why Aaron scared them

- No logic, but
- Artificial scarcity
- BitTorrent disobedience, emulation

## GOA legacy

- Alexandra Elbakyan, Sci-Hub
- Carl Malamud, P.R.O.
- PILIMI / Anna's library

## Economic importance

- Look at the profit margins and multiples
- Engines to make money out of money
- Money, power, exploiting capitalism
- The examples of Microsoft, Elsevier (~40 % margin)
- Organised robbery: workers, state, shareholders
- Stiglitz (2006)

## Global importance

- Colonialism is very much alive; war is the end game
- EU and PAC
- Example of South Africa and the blind
- Noglobal: WTO 1995, Seattle 1999, Genoa 2001
- War pause, Doha round
- More subtle: WIPO, ACTA (2008-2012), ISDS
- Current nationalist wave; EU vs. IRA

## Social importance

- Education and ignorance
- Open access and free knowledge matter
- Self-management of the commons/communities

## What you can do

Principles:

- Copyright nearly always harmful
- Use copyleft licenses unless (rare cases)
- Copy as much as you can
- Activism takes a long time

Examples:

- Upload to archive.org, scan ephemera
- Use open archives like Zenodo (Dissem.in helps)
- [POSSE](https://indieweb.org/POSSE)

## References

- Ostrom/Hess, Understanding knowledge as a commons (2007)
- Douglass North
- Aaron Swartz biographies
- Against intellectual monopolies
- Free Software, Free Society
